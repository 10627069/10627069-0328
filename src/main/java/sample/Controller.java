package sample;

import com.speedment.runtime.application.ApplicationBuilders;
import com.speedment.runtime.core.ApplicationBuilder;
import database.MydbApplication;
import database.MydbApplicationBuilder;
import database._.schema.city.City;
import database._.schema.city.CityImpl;
import database._.schema.city.CityManager;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

public class Controller {
    public Button btnDB;

    public void doConnect(ActionEvent actionEvent) {

    MydbApplication mydbApplication=new MydbApplicationBuilder().withLogging(MydbApplicationBuilder.LogType.CONNECTION)
            .withLogging(ApplicationBuilder.LogType.STREAM).withLogging(ApplicationBuilder.LogType.PERSIST)
            .withLogging(ApplicationBuilder.LogType.UPDATE).build();
    CityManager cityManager=mydbApplication.getOrThrow(CityManager.class);
    cityManager.stream().forEach(System.out::println);
    City city=new CityImpl().setCityname("台中市").setRowid(cityManager.stream().count()+1);
    cityManager.persist(city);
    }
}
